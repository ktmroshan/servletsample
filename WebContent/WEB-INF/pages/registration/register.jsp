<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container">
		<h2>Register User</h2>
		<p style="color: red;">${message}</p>
		<form action="${pageContext.request.contextPath}/register"
			method="post">
			<h4 class="text-danger">${error.message}</h4>
			<div class="form-group">
				<label for="email">Email:</label> <input type="text"
					class="form-control" id="email" value="${user.username}" placeholder="Enter email"
					name="username">
					<p class="text-danger">${error.username}</p>
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label> <input type="password"
					class="form-control" id="pwd" placeholder="Enter password"
					name="password">
					<p class="text-danger">${error.password}</p>
			</div>

			<div class="form-group">
				<label for="pwd">RePassword:</label> <input type="password"
					class="form-control" id="pwd" placeholder="Enter password"
					name="rePassword">
					<p class="text-danger">${error.rePassword}</p>
			</div>
	
			<button type="submit" class="btn btn-default">Register</button>
			<a style="float: right;"
				href="${pageContext.request.contextPath}/">Back To Login </a>
		</form>
	</div>

</body>
</html>