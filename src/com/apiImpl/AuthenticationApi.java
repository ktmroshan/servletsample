package com.apiImpl;

import javax.servlet.http.HttpSession;

import com.api.IAuthenticationApi;
import com.model.User;
import com.repository.UserRepository;

public class AuthenticationApi implements IAuthenticationApi {

	@Override
	public boolean login(String username, String password, HttpSession session) {

		try {
			User u = new User();
			u.setUsername(username);
			u.setPassword(password);

			UserRepository ar = new UserRepository();
			return ar.checkUserExist(u, session);
		} catch (Exception e) {
			return false;
		}

	}

}
