package com.apiImpl;

import java.sql.SQLException;

import com.api.IUserApi;
import com.model.User;
import com.repository.UserRepository;

public class UserApi implements IUserApi {

	@Override
	public User registerUser(User dto) throws SQLException {
		dto.setStatus(true);
		UserRepository ur = new UserRepository();
		dto=ur.registerUser(dto);
		return dto;
	}

	

}
