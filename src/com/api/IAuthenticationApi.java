package com.api;

import javax.servlet.http.HttpSession;

public interface IAuthenticationApi {

	boolean login(String username, String password, HttpSession session);;
}
