package com.api;

import java.sql.SQLException;

import com.error.UserError;
import com.model.User;

public interface IUserApi {
	
	User registerUser (User dto) throws SQLException;

}
