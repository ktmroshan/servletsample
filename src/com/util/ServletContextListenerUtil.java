package com.util;

import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.ServletContextEvent;

@WebListener
public class ServletContextListenerUtil implements ServletContextListener {

	public void contextInitialized(ServletContextEvent cse) {

		System.out.println("Application initialized");

	}

	public void contextDestroyed(ServletContextEvent cse) {

		System.out.println("Application shut down");

	}

}