package com.util;

import javax.servlet.http.HttpSession;

import com.model.User;

public class AuthenticationUtil {

	public static User getAuthentication(HttpSession session) {
		return (User) session.getAttribute("auth");
	}
}
