package com.validation;

import com.error.UserError;
import com.model.User;
import com.repository.UserRepository;

public class UserValidation {

	public UserError validateUserRegistration(User u) {
		UserError ue = new UserError();
		boolean valid = true;
		try {
			if (u == null) {
				valid = false;
				ue.setMessage("User Data Missing");
			} else {
				if (u.getUsername() == null || u.getUsername().trim().equals("")) {
					ue.setMessage("Registration Failed");
					ue.setUsername("Username Missing");
					valid = false;
				} else {
					UserRepository ur = new UserRepository();
					boolean exist = ur.checkUserExistByUserName(u.getUsername());
					if (exist) {
						ue.setMessage("Registration Failed");
						ue.setUsername("Username Already Exist With Username : " + u.getUsername());
						valid = false;
					}
				}

				if (u.getPassword() == null || u.getPassword().trim().equals("")) {
					ue.setMessage("Registration Failed");
					ue.setPassword("Password Missing");
					valid = false;
				} else if (u.getPassword().length() < 6 || u.getPassword().length() > 12) {
					ue.setMessage("Registration Failed");
					ue.setPassword("Password Must Between 6 to 12 Char");
					valid = false;
				}

				if (!u.getPassword().equals(u.getRePassword())) {
					ue.setMessage("Registration Failed");
					ue.setRePassword("Password and Repassword Doesn't Match");
					valid = false;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			ue.setMessage("Internal Server Error");
			valid = false;
		}
		ue.setValid(valid);
		return ue;

	}

}
