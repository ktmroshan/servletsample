package com.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;

import com.connection.ConnectionCon;
import com.model.User;

public class UserRepository {

	public boolean checkUserExist(User u, HttpSession session) throws SQLException {
		boolean valid = false;
		Connection con = null;
		try {
			con = ConnectionCon.getConnection();
			PreparedStatement ps = con
					.prepareStatement("select * from user where username=? and password=? and status=?");
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getPassword());
			ps.setBoolean(3, true);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				valid = true;
				u.setId(rs.getInt(1));
				u.setStatus(rs.getBoolean(4));
				session.setAttribute("auth", u);
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return valid;
	}

	public boolean checkUserExistByUserName(String username) throws SQLException {
		boolean valid = false;
		Connection con = null;
		try {
			con = ConnectionCon.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from user where username=?");
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				valid = true;
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return valid;
	}

	public User registerUser(User u) throws SQLException {
		boolean valid = false;
		Connection con = null;
		try {

			con = ConnectionCon.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO user (username, password,status) VALUES(?,?,?)",
					PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getPassword());
			ps.setBoolean(3, true);
			ps.executeUpdate();
			ResultSet res = ps.getGeneratedKeys();
			if (res.next()) {
				u.setId(res.getInt(1));
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
		}

		return u;
	}

}
