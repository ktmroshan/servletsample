package com.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.api.IAuthenticationApi;
import com.apiImpl.AuthenticationApi;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/login")
public class LoginController extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		System.out.println("USername : " + username + " PAssword :" + password);

		IAuthenticationApi api = new AuthenticationApi();

		boolean valid = api.login(username, password, request.getSession());

		if (valid) {

			System.out.println("Login Success");
			response.sendRedirect("/dashboard");

		} else {
			System.out.println("Login Failure");
			request.setAttribute("message", "Username Or Password Wrong");
			RequestDispatcher dis = request.getRequestDispatcher("/");
			dis.forward(request, response);
		}

	}

}
