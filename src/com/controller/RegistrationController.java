package com.controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.api.IUserApi;
import com.apiImpl.UserApi;
import com.error.UserError;
import com.model.User;
import com.validation.UserValidation;

/**
 * Servlet implementation class RegistrationController
 */
@WebServlet("/register")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dis = request.getRequestDispatcher("WEB-INF/pages/registration/register.jsp");
		dis.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User u = paramConverter(request);
		UserValidation uv = new UserValidation();
		UserError ue = uv.validateUserRegistration(u);
		if (ue.isValid()) {
			IUserApi ua = new UserApi();
			try {
				ua.registerUser(u);
				request.setAttribute("message", "Registration Success. Please Login");
				RequestDispatcher rd = request.getRequestDispatcher("/");
				rd.forward(request, response);
			} catch (SQLException e) {
				ue.setMessage("Internal Server Error");
				ue.setValid(false);
				request.setAttribute("error", ue);
				request.setAttribute("user", u);
				doGet(request, response);
				e.printStackTrace();
			}

		} else {
			request.setAttribute("error", ue);
			request.setAttribute("user", u);
			doGet(request, response);
		}

	}

	private User paramConverter(HttpServletRequest request) {
		User u = new User();
		if (request.getParameter("username") != null) {
			u.setUsername(request.getParameter("username"));
		}
		if (request.getParameter("password") != null) {
			u.setPassword(request.getParameter("password"));
		}
		if (request.getParameter("rePassword") != null) {
			u.setRePassword(request.getParameter("rePassword"));
		}
		return u;

	}

}
